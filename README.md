# Ada_Basics

Tutorial of foundational concepts in Ada programming language. Most code taken from: https://learn.adacore.com/courses/intro-to-ada/index.html . Some additional code added by me.