-- Tutorial on basics of strong typing variables in Ada.
with Ada.Text_IO;

procedure Main is

   -- Basic integer type---
      procedure integer_type_example is
      type My_int is range -1 .. 20;
      -- Types must appear in this declarative region
   begin
      for i in My_int loop
         Ada.Text_IO.Put(My_int'Image(i));
      end loop;
   end integer_type_example;
   ------------------------

   -- Unsigned Type---------
   procedure unsigned_type_example is
   type Mod_Int is mod 2 ** 5;
   --              ^ Range is 0 .. 31

   A : Mod_Int := 20;
   B : Mod_Int := 15;
   C : Mod_Int := 9;
   M : Mod_Int := A + B;
   --  No overflow here, M = (20 + 15) mod 32 = 3
   begin
      Ada.Text_IO.Put_Line("M: " &Mod_Int'Image(M));
      for I in 1 .. M loop
         Ada.Text_IO.Put_Line ("Hello, World!");
      end loop;

   end unsigned_type_example;

   --------------------------

   -- Enumerations-------------------
   procedure enumeration_example is
      type Days is (Monday, Tuesday, Wednesday, Thursday,
                    Friday, Saturday, Sunday);
   begin
      for i in Days loop
         Ada.Text_IO.Put("The day is: "& Days'Image(i) &": ");
         case i is
            when Monday .. Friday =>
               Ada.Text_IO.Put_Line("Weekday");
            when Saturday .. Sunday =>
               Ada.Text_IO.Put_Line("Freakin Weekend");
         end case;
      end loop;
   end enumeration_example;
   ----------------------------------


   -- Custom Float types----------------
   procedure Custom_Floating_Types_example is
      type T3  is digits 3;
      type T15 is digits 15;
      type T18 is digits 18;
   begin
      Ada.Text_IO.Put_Line ("T3  requires " & Integer'Image (T3'Size) & " bits");
      Ada.Text_IO.Put_Line ("T15 requires " & Integer'Image (T15'Size) & " bits");
      Ada.Text_IO.Put_Line ("T18 requires " & Integer'Image (T18'Size) & " bits");
   end Custom_Floating_Types_example;
   --------------------------------------


   -- Floating point range --------------
   procedure Floating_Point_Range_example is
   type T_Norm  is new Float range -1.0 .. 1.0;
   A  : T_Norm;
   begin
      A := 1.0;
      Ada.Text_IO.Put_Line ("The value of A is " & T_Norm'Image (A));
   end Floating_Point_Range_example;

   -------------------------------------


   -- Type conversion ------------------
   procedure Convert_type_example is
   type Meters is new Float;
   type Miles is new Float;
   Dist_Imperial : Miles;
   Dist_Metric : constant Meters := 100.0;
   begin
      Dist_Imperial := (Miles (Dist_Metric) * 1609.0) / 1000.0;
      --                ^ Type conversion, from Meters to Miles
   --  Now the code is correct

   Ada.Text_IO.Put_Line (Miles'Image (Dist_Imperial));
end Convert_type_example;
   -------------------------------------


   -- Type conversion with function ------
   procedure Conv_with_function_example is
   type Meters is new Float;
   type Miles is new Float;

   --  Function declaration, like procedure but returns a value.
   function To_Miles (M : Meters) return Miles is
   --                             ^ Return type
   begin
      return (Miles (M) * 1609.0) / 1000.0;
   end To_Miles;

   Dist_Imperial : Miles;
   Dist_Metric   : constant Meters := 100.0;
begin
   Dist_Imperial := To_Miles (Dist_Metric);
   Ada.Text_IO.Put_Line (Miles'Image (Dist_Imperial));
end Conv_with_function_example;

   ---------------------------------------

   -- Derived Types ----------------------
   procedure derived_type_example is
   --  ID card number type, incompatible with Integer.
   type Social_Security_Number
   is new Integer range 0 .. 999_99_9999;
   --                   ^ Since a SSN has 9 digits max, and cannot be
   --                     negative, we enforce a validity constraint.

   SSN : Social_Security_Number := 555_55_5555;
   --                              ^ You can put underscores as formatting in
   --                                any number.

   I   : Integer;

   --Invalid : Social_Security_Number := -1;
   --                                  ^ This will cause a runtime error
   --                                    (and a compile time warning with
   --                                     GNAT)
   begin
      --I := SSN;                           -- Illegal, they have different types
      --SSN := I;                           -- Likewise illegal
      I := Integer (SSN);                 -- OK with explicit conversion
      SSN := Social_Security_Number (I);  -- Likewise OK
      Ada.Text_IO.Put_Line("SSN: "&Social_Security_Number'Image(SSN));
   end derived_type_example;
   --------------------------------------

   -- Subtype example--------------------
   procedure subtype_example is
   type Days is (Monday, Tuesday, Wednesday, Thursday,
                 Friday, Saturday, Sunday);

   --  Declaration of a subtype
   subtype Weekend_Days is Days range Saturday .. Sunday;
   --                           ^ Constraint of the subtype

   M : Days := Sunday;

   S : Weekend_Days := M;
   --  No error here, Days and Weekend_Days are of the same type.
begin
   for I in Days loop
      case I is
            --  Just like a type, a subtype can be used as a range

         when Weekend_Days =>
            Put_Line ("Week end!");
         when others =>
            Put_Line ("Hello on " & Days'Image (I));
      end case;
   end loop;
end subtype_example;

   --------------------------------------
begin
   --integer_type_example;
   --unsigned_type_example;
   --enumeration_example;
   --Custom_Floating_Types_example;
   --Floating_Point_Range_example;
   --Convert_type_example;
   --Conv_with_function_example;
   --derived_type_example;
   subtype_example;




end Main;
